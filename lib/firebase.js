import admin from 'firebase-admin';
// TODO might need to remove replace(/\\n/g, '\n') as its wasn't there before 
if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert({
      client_email: process.env.FIREBASE_CLIENT_EMAIL,
      private_key: process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
      project_id: 'nath-blog'
    }),
    databaseURL: 'https://nath-blog-default-rtdb.firebaseio.com/'
  });
}

export default admin.database();
