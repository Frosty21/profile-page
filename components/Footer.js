import Link from 'next/link';
// TODO add spotify now playing later
import NowPlaying from '@/components/NowPlaying';

const ExternalLink = ({ href, children }) => (
  <a
    className="text-gray-500 hover:text-gray-600 transition"
    target="_blank"
    rel="noopener noreferrer"
    href={href}
  >
    {children}
  </a>
);

const FooterLink = ({ href, children }) => (
  <Link href={href}>
    <a
      className="text-gray-500 hover:text-gray-600 transition"
    >
      {children}
    </a>
  </Link>
)

export default function Footer() {
  return (
    <footer className="flex flex-col justify-center items-start max-w-2xl mx-auto w-full mb-8">
      <hr className="w-full border-1 border-gray-200 dark:border-gray-800 mb-8" />
      {/* <NowPlaying /> */}
      <div className="w-full max-w-2xl grid grid-cols-1 gap-4 pb-16 sm:grid-cols-3">
        <div className="flex flex-col space-y-4">
          <FooterLink href="/" children="Home" />
          <FooterLink href="/about" children="About" />
          <FooterLink href="/newsletter" children="Newsletter" />
          <ExternalLink href="/nathan-froese-resume.pdf" children="Resume" />
        </div>
        <div className="flex flex-col space-y-4">
          <ExternalLink href="https://twitter.com/nathanfroese21" children="Twitter" />
          <ExternalLink href="https://github.com/frosty21" children="GitHub" />
          <ExternalLink href="https://gitlab.com/frosty21" children="GitLab" />
          <ExternalLink href="https://www.youtube.com/channel/UC1p36TT5QimoHLDom1dg2AQ" children="YouTube" />
        </div>
        <div className="flex flex-col space-y-4">
          <FooterLink href="/uses" children="Uses" />
          {/* <FooterLink href="/guestbook" children="Guestbook" /> */}
          <FooterLink href="/snippets" children="Snippets" />
          {/* <FooterLink href="/tweets" children="Tweets" /> */}
        </div>
      </div>
    </footer>
  );
}
