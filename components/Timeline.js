import { useState } from 'react';

const Divider = () => {
  return (
    <div className="border border-gray-200 dark:border-gray-600 w-full my-8" />
  );
};

const Year = ({ children }) => {
  return (
    <h3 className="text-lg md:text-xl font-bold mb-4 tracking-tight text-gray-900 dark:text-gray-100">
      {children}
    </h3>
  );
};

const Step = ({ title, children }) => {
  return (
    <li className="mb-4 ml-2">
      <div className="flex items-center mb-2 text-green-700 dark:text-green-300">
        <span className="sr-only">Check</span>
        <svg className="h-4 w-4 mr-2" viewBox="0 0 24 24">
          <g
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path d="M22 11.08V12a10 10 0 11-5.93-9.14" />
            <path d="M22 4L12 14.01l-3-3" />
          </g>
        </svg>
        <p className="font-medium text-gray-900 dark:text-gray-100">{title}</p>
      </div>
      <p className="text-gray-700 dark:text-gray-400 ml-6">{children}</p>
    </li>
  );
};

const FullTimeline = () => (
  <>
    <Divider />
    <Year>2015-2016</Year>
    <ul>
      <Step title="Moved to oh Canada 🇨🇦🤘🏻">
        It was a big move to goto Canada Vancouver
      </Step>
      <Step title="Joined Ledcor">
        I had an opportunity in working as a fiber design engineer
      </Step>
    </ul>
    <Divider />
    <Year>2013-2014</Year>
    <ul>
      <Step title="Graduated College 🎓">
        One of my most cherished accomplishments. I worked my ass off to get
        this degree. Couldn't of done it without the support of family and friends
      </Step>
      <Step title="Second Internship and First Job">
        Working at Schlumberger one of the biggest oil and gas service companies as a Financial Asset Coordinator
        was a huge undertaking and made me realize the need for having coding skills.
      </Step>
    </ul>
    <Divider />
    <Year>2010</Year>
    <ul>
      <Step title="First Internship">
        Spent a year working at Total Marine Tech as a electronics technician
      </Step>
    </ul>
    <Divider />
    <Year>2008</Year>
    <ul>
      <Step title="Started Uni at Murdoch University">
        I went to a university a little further away from home MDU in Electrical and Computer systems Engineering
      </Step>
      <Step title="Landed First Job">
        Finally making my own money but only as a pizza boy... who cant love 🍕🍕🍕
      </Step>
    </ul>
    <Divider />
    <Year>2007</Year>
    <ul>
      <Step title="Graduated High School">
        My hometown school at Christ Church Grammar School Graduated.
        Wished I took programming in highschool.
      </Step>
      <Step title="Had a passion for Sports">
        Played tennis, field hockey, rowing and many more sports.
      </Step>
    </ul>
    <Divider />
    <Year>2000</Year>
    <ul>
      <Step title="First Gameboy">
        First portable console played Zelda, Pokemon and many more
      </Step>
    </ul>
    <Divider />
    <Year>1999</Year>
    <ul>
      <Step title="First Computer">
        I remember many nights playing Age of Empires, Command and Conquer, and
        Runescape.
      </Step>
    </ul>
    <Divider />
    <Year>1990</Year>
    <ul>
      <Step title="Born 🚼👶🏼🍼" />
    </ul>
  </>
);

export default function Timeline() {
  const [isShowingFullTimeline, showFullTimeline] = useState(false);

  return (
    <>
      <h3 className="font-bold text-2xl md:text-4xl tracking-tight mb-4 mt-8 text-black dark:text-white">
        Timeline
      </h3>
      <Year>2021-2022</Year>
      <ul>
        <Step title="The 2021 Dev Roadmap 🗺️">
          Learning new libraries in React with NextJS, GraphQL, GQL Code Generator, Typescript and two state managements(React-Query, SWR or Apollo).
          Databases Redis,FaunaDB and MongoDB
          Other ones I'll try later are Vue, Vuex and NuxtJS.
        </Step>
      </ul>
      <Divider />
      <Year>2020</Year>
      <ul>
        <Step title="Moved in with my Girl 🥳">
          Amongst the COVID chaos I decided to make the move from Vancouver to live with my girlfriend after
          hitting 2yrs together.
        </Step>
      </ul>
      <Divider />
      <Year>2018-2019</Year>
      <ul>
        <Step title="Worked as a Locksmith 🔐">
          Worked at AlScott Lock and Safe as locksmith technician. My bucket list of trades to build a house.
          Has come in handy when I have forgotten my house key or someone has locked their car door.
        </Step>
      </ul>
      <Divider />
      <Year>2017</Year>
      <ul>
        <Step title="My first web dev gig @ Gomae 😃">
          Met a couple of cool dudes wanting to design an app for creating healthy restaurant meal choices.
          It was definitely a hit amongst the Vancouver community.
        </Step>
      </ul>
      {isShowingFullTimeline ? (
        <FullTimeline />
      ) : (
        <button
          type="button"
          className="flex items-center text-sm my-4 mx-auto px-4 py-2 rounded-md font-medium text-gray-900 dark:text-gray-100"
          onClick={() => showFullTimeline(true)}
        >
          See More
          <svg
            className="h-4 w-4 ml-1"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M19 9l-7 7-7-7"
            />
          </svg>
        </button>
      )}
    </>
  );
}
