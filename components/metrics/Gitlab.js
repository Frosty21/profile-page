import useSWR from 'swr';
import format from 'comma-number';

import fetcher from '@/lib/fetcher';
import MetricCard from '@/components/metrics/Card';

export default function Gitlab() {
  const { data } = useSWR('/api/gitlab', fetcher);

  const issues = format(data?.issues);
  const link = 'https://gitlab.com/frosty21/';

  return <MetricCard header="Gitlab Issues" link={link} metric={issues} />;
}
