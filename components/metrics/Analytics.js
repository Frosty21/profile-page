import useSWR from 'swr';
import format from 'comma-number';

import fetcher from '@/lib/fetcher';
import MetricCard from '@/components/metrics/Card';

export default function Analytics() {
  const { data } = useSWR('/api/views', fetcher);

  const pageViews = format(data?.total);
  // TODO to change to production site url
  const link = 'http://localhost:3000/';

  return <MetricCard header="All-Time Views" link={link} metric={pageViews} />;
}
