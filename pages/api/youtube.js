import { google } from 'googleapis';

import googleAuth from '@/lib/google/auth';

export default async (_, res) => {
  const auth = await googleAuth.getClient();
  const youtube = google.youtube({
    auth,
    version: 'v3'
  });

  const response2 = await youtube.subscriptions.list({
    id: 'UC1p36TT5QimoHLDom1dg2AQ',
    part: 'snippet,contentDetails'
  })

  const response = await youtube.channels.list({
    id: 'UC1p36TT5QimoHLDom1dg2AQ',
    part: 'statistics'
  });

  const channel = response.data.items[0];
  const channelSubs = response2
  const { subscriberCount, viewCount } = channel.statistics;

  res.setHeader(
    'Cache-Control',
    'public, s-maxage=1200, stale-while-revalidate=600'
  );

  return res.status(200).json({
    subscriberCount,
    viewCount,
    channelSubs
  });
};
