export default async (_, res) => {
  const profilePageProjectIssues = await fetch('https://gitlab.com/api/v4/projects/26142425/issues_statistics');
  userStats = await profilePageProjectIssues.json();

  res.setHeader(
    'Cache-Control',
    'public, s-maxage=1200, stale-while-revalidate=600'
  );

  return res.status(200).json({
    issues: userStats.counts.opened
  });
};
