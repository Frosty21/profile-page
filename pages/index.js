import Link from 'next/link';
import { google } from 'googleapis';

import googleAuth from '@/lib/google/auth';
import Timeline from '../components/Timeline';
import Container from '../components/Container';
import BlogPost from '../components/BlogPost';
import Subscribe from '../components/Subscribe';
import ProjectCard from '../components/ProjectCard';
import VideoCard from '../components/VideoCard';

export async function getStaticProps() {
  const auth = await googleAuth.getClient();
  const youtube = google.youtube({
    auth,
    version: 'v3'
  });

  // TODO change youtube ID# 1p36TT5QimoHLDom1dg2AQ once there are some videos in my channel
  const response = await youtube.videos.list({
    id: 'Pd2tVxhFnO4,FytxaSVQROc,u_o09PD_qAs',
    part: 'snippet,statistics'
  });

  return {
    props: {
      videos: response.data.items
    },
    revalidate: 60 * 60 // 1 hour
  };
}

export default function Home({ videos }) {
  return (
    <Container>
      <div className="flex flex-col justify-center items-start max-w-2xl mx-auto mb-16">
        <h1 className="font-bold text-3xl md:text-5xl tracking-tight mb-4 text-black dark:text-white">
          G'Day, I’m Nathan Froese
        </h1>
        <h2 className="prose text-gray-600 dark:text-gray-400 mb-16">
          I’m a developer, sportsman 🏑🎾🏄, foodie explorer 🍔🍕🌮, coffee addict☕ and hobbyist sometimes. I worked at Gomae as a Frontend
          Dev. You’ve found my personal plate of the internet –&nbsp;
          {/* <Link href="/guestbook">
            <a>sign my guestbook&nbsp;</a>
          </Link>
          while you're having a gander. */}
        </h2>
        <h3 className="font-bold text-2xl md:text-4xl tracking-tight mb-4 text-black dark:text-white">
          Most Popular
        </h3>
        <BlogPost
          title="Everything I Know About Style Guides, Design Systems, and Component Libraries"
          summary="A deep-dive on everything I've learned in the past year building style guides, design systems, component libraries, and their best practices."
          slug="style-guides-component-libraries-design-systems"
        />
        <h3 className="font-bold text-2xl md:text-4xl tracking-tight mb-4 mt-8 text-black dark:text-white">
          Courses I recommend
        </h3>
        <ProjectCard
          title="React 2025"
          description="Build and deploy a modern Jamstack application using the most popular open-source software."
          href="https://react2025.com/"
          icon="react2025"
        />
        <ProjectCard
          title="Learn Next.js"
          description="A free video course for building static and server-side rendered applications with Next.js and React."
          href="https://masteringnextjs.com/"
          icon="nextjs"
        />
        {/* Todo change from Fastfeedback to React SVG icon */}
        <ProjectCard
          title="Learn ReactJS EBook"
          description="A free ebook selection for understanding the principles in ReactJS."
          href="https://github.com/EbookFoundation/free-programming-books/blob/master/courses/free-courses-en.md#react"
          icon="react"
        />
        <h3 className="font-bold text-2xl md:text-4xl tracking-tight mb-4 mt-12 text-black dark:text-white">
          Recent Videos
        </h3>
        {videos.map((video) => (
          <VideoCard key={video.id} {...video} />
        ))}
        <Timeline />
        <Subscribe />
      </div>
    </Container>
  );
}
