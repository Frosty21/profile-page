import Link from 'next/link';

import Container from '@/components/Container';

const Talk = ({ title, link, children }) => (
  <>
    <h3 className="font-medium mb-2 text-lg">
      <a
        className="flex items-center text-gray-900 dark:text-gray-100"
        target="_blank"
        rel="noopener noreferrer"
        href={link}
      >
        {title}
        <div>
          <svg
            className="h-4 w-4 ml-1"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"
            />
          </svg>
        </div>
      </a>
    </h3>
    <p className="text-gray-600 dark:text-gray-400 mb-8">{children}</p>
  </>
);

export default function About() {
  return (
    <Container title="About – Nathan Froese">
      <div className="flex flex-col justify-center items-start max-w-2xl mx-auto mb-16">
        <h1 className="font-bold text-3xl md:text-5xl tracking-tight mb-4 text-black dark:text-white">
          About Me
        </h1>
        <div className="mb-8 prose leading-6 text-gray-600 dark:text-gray-400">
          <p>
            G'Day, I’m Nathan. I'm a developer novice, sportsman, and explorer of content in the JavaScript sphere.
            I have worked at&nbsp;
            <a href="https://gomaemealprep.com/" target="_blank" rel="noopener noreferrer">
              🥣Gomae
            </a>
            &nbsp;as a Junior Frontend Dev and am actively searching and engaging for further opportunities to develop my skills.
          </p>
          <p>
            I’ve been on a journey exploring and discovering new connections mostly virtually at conferences and meetups about
            front-end development, design and networking. I hope grasp further knowledge and wisdom in
            development and tech careers&nbsp;(
            <a href="/nathan-froese-resume.pdf" target="_blank" rel="noopener noreferrer">
              my resume
            </a>
            ), towards my personal life on my journal.&nbsp;
            {/* TODO later make this available */}
            {/* <Link href="/newsletter">
              <a>my journal.</a>
            </Link> */}
          </p>
          <p>
            I grew up most of my life in sunny beaches of Perth Australia🇦🇺, graduating in
            Electrical and Industrial Computer Systems Engineering.
            I've moved from Australia🇦🇺 ✈️ Canada🇨🇦 to explore and discover web tech industry. I spend my free
            time playing sports, recreating recipes, and enjoy hanging out with friends
            and family in both 🇦🇺🇨🇦.
          </p>
        </div>
        <h2 className="font-bold text-3xl tracking-tight mb-4 text-black dark:text-white">
          Conference / Live talks been to
        </h2>
        <Talk
          title="Why Images Hurt App Performance & How the New Next.js Image Component Can Help"
          link="https://youtu.be/3HotNkwgz2Q?list=PLBnKlKpPeagnT2Cmv4giCbosfrbKnuYTD"
        >
          Alex Castle from Google talks about the teaming up with Next team to improve Image performance on NextJS. Core Web Vitals
          (LCP, FID and CLS) with loading, interactivity and visual stability have large effect on image performance.
          Solutions to this are Lazy loading, pre-loading depending on priority and self optimizes photos to best format.
        </Talk>
        <Talk
          title="The Future of JAMSTACK changing with DXP"
          link="https://youtu.be/YEzAod1sDdg"
        >
          During Tim Benniks talk at JSWorld conference gave a overall discussion into Digital Experience Platform (DXP).
          With DXP gives you the best breed vs suite systems using the sandwich analogy.
          Breed or suite maybe better depending on your ingredients and characteristics needed.
        </Talk>
        <Talk
          title="CSR / SSR / SSG / ISR with Next.js – Explained!"
          link="https://youtu.be/eMwTEo6AjDc"
        >
          Lee Robinson explains Client Site Rendering (CSR), Server Side Rendering (SSR), Static Site Generating (SSG)
          and Incremental Static Regeneration (ISR). Huge benefits of ISR allowing static connect to be regenerated once stale.
          When you would use one over the other depends on what you are trying to measure.
        </Talk>
        <iframe
          height="280"
          src="https://www.google.com/maps/d/embed?mid=1VlyV2021knbTK8V1D7Av6T1zjOxZu2Pr&hl=en"
          title="Naths Travel Map"
          width="100%"
        />
      </div>
    </Container>
  );
}
