export default [
  {
    date: 'February 16, 2020',
    publisher: 'Knut Melvær',
    title: 'on-the-limits-of-mdx',
    url:
      'https://www.knutmelvaer.no/blog/2020/02/on-the-limits-of-mdx/'
  },
  {
    date: 'December 27, 2020',
    publisher: 'Nirmalya',
    title: 'Difference between SWR and React Query in terms of fetching data',
    url:
      'https://codebrahma.com/difference-between-swr-and-react-query/'
  },
  {
    date: 'August 20, 2015',
    publisher: 'Lee Robinson',
    title: 'Past, Present, and Future of React State Management',
    url:
      'https://leerob.io/blog/react-state-management'
  }
];
