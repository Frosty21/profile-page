[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/git/external?repository-url=https%3A%2F%2Fgitlab.com%2FFrosty21%2Fprofile-page)

# nathfroese.vercel.app

My portfolio has given me a way to express myself - a layout from Lee Rob, using the API, and finally to Next.js/React/MDX. My personal page of the internet provides a platform for my expression's, views and to showcase my work.

If you want to understand the layout of what I used watch lee rob's [one-hour live stream](https://www.youtube.com/watch?v=xXQsF0q8KUg) explaining the codebase.

## Overview

- `pages/api/*` - [API routes](https://nextjs.org/docs/api-routes/introduction) powering [`/dashboard`](https://nathfroese.vercel.app/dashboard), newsletter subscription, and post views.
- `pages/blog/*` - Static pre-rendered blog pages using [MDX](https://github.com/mdx-js/mdx).
- `pages/dashboard` - [Personal dashboard](https://nathfroese.vercel.app/dashboard) containing metrics like spotify music, views, and subscribers.
- `pages/*` - All other static pages.

## API Reference

#### Get all items

```http
  GET /api/items
```

| Parameter                    | Type          | Description                                                                                                       |
| :--------------------------- | :------------ | :---------------------------------------------------------------------------------------------------------------- |
| `UNSPLASH_ACCESS_KEY`        | `string`      | **Required**. API key for photos                                                                                  |
| `GOOGLE_ENCRYPTION_KEY`      | `AES-CBC-128` | **Required**. API key for Google API access                                                                       |
| `GOOGLE_ENCRYPTION_VI`       | `AES-CBC-128` | **Required**. API key for Google API access                                                                       |
| `BUTTONDOWN_API_KEY`         | `string`      | **Optional**. API key for Subscribing users access on newsletters and new content                                 |
| `NEXT_PUBLIC_FATHOM_SITE_ID` | `string`      | **Optional**. API key for gathering stats without tracers. options are fathom or google analytics                 |
| `SPOTIFY_CLIENT_ID`          | `string`      | **Required**. API key for Spotify Music                                                                           |
| `SPOTIFY_CLIENT_SECRET`      | `string`      | **Required**. API key for Spotify Server Side                                                                     |
| `SPOTIFY_REFRESH_TOKEN`      | `string`      | **Required**. API key for Spotify Refreshing Access token                                                         |
| `REDIS_URL`                  | `string`      | **Optional**. API key for storing guestbook comments into the redis database. might switch to Notion API database |
| `SESSION_KEY`                | `string`      | **Optional**. API key for access                                                                                  |
| `GOOGLE_ENCRYPTION_VI`       | `string`      | **Required**. API key for Google API access                                                                       |

## Running Locally

```bash
$ git clone https://gitlab.com/Frosty21/profile-page.git
$ cd profile-page
$ yarn
$ yarn dev
```

Create a `.env.local` file similar to [`.env.example`](https://gitlab.com/Frosty21/profile-page/-/blob/master/.env.example).

## Future Ideas

- Adding tags into posts
- include comments section under posts
- link to hackernoon and dev.to for better community exposure
- add link to twitch stream

## Built Using

- [Next.js](https://nextjs.org/)
- [Vercel](https://vercel.com)
- [MDX](https://github.com/mdx-js/mdx)
- [Tailwind CSS](https://tailwindcss.com/)

## Acknowledgements

- [Leerob.io](https://leerob.io/)
